module.exports = {
    // options...
	// Challenge: Invalid HTTP_HOST header in Docker (issue-Done-12022-08-15, overcome-Done-12022-08-15)
	// https://github.com/cookiecutter/cookiecutter-django/issues/879
	// https://github.com/gitpod-io/gitpod/issues/26 (overcome-Done-12022-08-15, rate-4-Stars)
	// https://stackoverflow.com/questions/65643544/how-fix-invalid-host-header-error-in-vue-3-project
    devServer: {
       disableHostCheck: true
    }
}
